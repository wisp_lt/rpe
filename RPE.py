expr = []
stack = []
operations = {'+': 2, '-': 2, '*': 2, '/': 2, '!': 1, '=': 2}
key_words = {'repeat', 'print'}
variables = {}


def is_digit(str):
    try:
        float(str)
        return True
    except ValueError:
        return False


def get_value(token):
    if is_digit(token):
        return float(token)
    elif variables.__contains__(token):
        val = variables[token]
        if val != "Undefined":
            return val
        else:
            print("Undefined variable \"" + token + "\"")
            quit(1)


def fac(n):
    res = 1
    for i in range(1, int(n) + 1):
        res *= i
    return res

def calculate(op, count):
    args = []
    for i in range(count):
        args.append( stack.pop() )

    if op == '+':
        return str(get_value(args[1]) + get_value(args[0]))
    elif op == '-':
        return str(get_value(args[1]) - get_value(args[0]))
    elif op == '*':
        return str(get_value(args[1]) * get_value(args[0]))
    elif op == '/':
        return str(get_value(args[1]) / get_value(args[0]))
    elif op == '!':
        return str(fac(get_value(args[0])))
    elif op == '=':
        if not args[1] in variables:
            print("Operation \'=\' is not defined for numbers")
            quit(1)
        else:
            a = args[1]
            variables[a] = get_value(args[0])
            return a

def process(line):
    for i in range(len(line)):
        token = line[i]
        if is_digit(token):
            stack.append(token)
        elif token.isalpha():
            if token in key_words:
                if token == "repeat":
                    loop(get_value(line[i + 1]))
                    break
                elif token == "print":
                    print( get_value(line[i + 1]) )
            elif not token in variables:
                variables[token] = "Undefined"
            stack.append(token)
        elif token in operations:
            stack.append(calculate(token, operations[token]))
        else:
            print("Undefined token \'" + token + "\'")
            quit(1)

def loop(count):
    code = []
    line = input()
    while line != "end":
        code.append(line.split(" "))
        line = input()

    for i in range(int(count)):
        for line in code:
            process(line)

def start():
    expr = input()

    while expr != "exit":
        expr = expr.split(" ")
        process(expr)
        expr = input()

start()
